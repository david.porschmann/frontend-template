module.exports = function(grunt) {
    grunt.initConfig({

        concat: {
            options: {
                separator: '\n\n//-------------------------------------------\n\n',
                banner: '//-------------------Porschmani------------------------\n\n'
            },
            dist: {
                src: ['components/scripts/*.js'],
                dest: 'builds/development/assets/js/script.js'
            },
            jquery: {
                src: ['node_modules/jquery/dist/jquery.min.js'],
                dest: 'builds/development/assets/js/jquery.min.js'
            },
            bootstrap: {
                src: ['node_modules/bootstrap/dist/js/bootstrap.bundle.js'],
                dest: 'builds/development/assets/js/bootstrap.js'
            }
        },

        sass: {
            dist: {
                options: {
                    style: 'compact'
                    // style: 'expanded'
                    // style: 'nested'
                    // style: 'compressed'
                },
                files: {
                    'builds/development/assets/css/main.css': 'components/sass/main.scss',
                }
            },
            bootstrap: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'builds/development/assets/css/bootstrap.css': 'node_modules/bootstrap/scss/bootstrap.scss'
                }
            }
        },

        postcss: {
            options: {
                map: true,
                processors: [
                    require('autoprefixer')({browsers: ['last 2 versions']}),
                    //require('cssnano')()
                ]
            },
            dist: {
                src: 'builds/development/assets/css/*.css'
            }
        },

        connect: {
          server: {
              options: {
                  hostname: 'localhost',
                  port: 3000,
                  base: 'builds/development',
                  livereload: true
              }
          }
        },

        watch: {
            options: {
                spawn: false,
                livereload: true
            },
            scripts: {
                files: [
                    'builds/development/**/*.html',
                    'components/scripts/**/*.js',
                    'components/sass/**/*.scss'
                ],
                tasks: ['concat:dist', 'sass:dist', 'postcss']
            }
        },
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');

    // Setup project with all files run:
    // grunt setup
    grunt.registerTask('setup', ['concat', 'sass']);

    // Run default grunt and watch files + autoreload run:
    // grunt
    grunt.registerTask('default', ['concat:dist', 'sass:dist', 'postcss', 'connect', 'watch']);

};