# Frontend template based on grunt;

## Version X1.0.0
---------------------------------

### This setup is created and will work only if you already have installed node.js <br /> Check your version in terminal <br /> with: `node --version`
<br />
If you receive a version number continue with following steps:


> 1 Start <br />
Clone project `git clone https://gitlab.com/david.porschmann/frontend-template.git`

> 2 CD into your project <br />
Check path with `pwd`

> 3 Install <br />
Run: `npm i or npm install`

- This will install your local project grunt and all dependencies

> 4 Create files<br />
Run: `grunt setup`

- This will compile the jquery and bootstrap files

> 5 Start working <br />
Run: `grunt`

- This will run a localhost on port 3000
- Create a watch whenever a file is changed and update the project

All changes can be configured in the gruntfile.js<br />

[update 1.0.0 - 21-08-2019]: <br />
Added scripts dir in components directory. ( that folder was missing )<br /> 
scss files should begin with a '_' underscore & <br />
included in the '_index.scss' of that folder.

Feel free to share<br />
Porschmann David
